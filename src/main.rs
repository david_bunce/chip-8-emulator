extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;
use std::env;
use std::fs;
use std::io::Result;
use std::io::Read;

pub mod app;

use crate::app::chip_8::Chip8;

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        //
    }

    fn update(&mut self, args: &UpdateArgs) {

    }
}

fn main() {

    let args: Vec<String> = env::args().collect();
    let filepath = args.get(1);

    let mut chip8 = Chip8::new();

    match filepath {
        Some(path) => {
            chip8.open_game_file(path);
            chip8.launch_game();
        },
        None => {
            panic!("Sorry, no filepath given for game.");
        }
    };
    
    // let opengl = OpenGL::V3_2;

    // let mut window: Window = WindowSettings::new("spinning-square", [200, 200])
    //     .graphics_api(opengl)
    //     .exit_on_esc(true)
    //     .build()
    //     .unwrap();

    // let mut app = App {
    //     gl: GlGraphics::new(opengl)
    // };

    // let mut events = Events::new(EventSettings::new());
    // while let Some(e) = events.next(&mut window) {
    //     if let Some(args) = e.render_args() {
    //         app.render(&args);
    //     }

    //     if let Some(args) = e.update_args() {
    //         app.update(&args);
    //     }
    // }
}
