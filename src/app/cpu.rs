use piston::Key;
use std::collections::HashMap;
use std::collections::hash_map::Iter;

use crate::app::stack::Stack;
use crate::app::keymapping::KeyMapping;
use crate::app::register::{CPURegister, ByteRegister, WordRegister};

pub struct CPU {
    registers: HashMap<CPURegister, ByteRegister>,
    i_register: WordRegister,
    program_counter: u16,
    delay_timer: u8,
    sound_timer: u8,
    stack: Stack,
    keyboard: KeyMapping
}

impl CPU {
    pub fn new() -> CPU {
        let mut map = HashMap::new();
        map.insert(CPURegister::V0, ByteRegister::new());
        map.insert(CPURegister::V1, ByteRegister::new());
        map.insert(CPURegister::V2, ByteRegister::new());
        map.insert(CPURegister::V3, ByteRegister::new());
        map.insert(CPURegister::V4, ByteRegister::new());
        map.insert(CPURegister::V5, ByteRegister::new());
        map.insert(CPURegister::V6, ByteRegister::new());
        map.insert(CPURegister::V7, ByteRegister::new());
        map.insert(CPURegister::V8, ByteRegister::new());
        map.insert(CPURegister::V9, ByteRegister::new());
        map.insert(CPURegister::VA, ByteRegister::new());
        map.insert(CPURegister::VB, ByteRegister::new());
        map.insert(CPURegister::VC, ByteRegister::new());
        map.insert(CPURegister::VD, ByteRegister::new());
        map.insert(CPURegister::VE, ByteRegister::new());
        map.insert(CPURegister::VF, ByteRegister::new());
        map.insert(CPURegister::VX, ByteRegister::new());

        CPU {
            registers: map,
            i_register: WordRegister::new(),
            program_counter: 0,
            delay_timer: 0,
            sound_timer: 0,
            stack: Stack::new(),
            keyboard: KeyMapping::new()
        }
    }

    pub fn register(&self, register: &str) -> Option<&ByteRegister> {
        let r = CPURegister::from_string_letter(register);
        self.registers.get(&r)
    }

    pub fn get_register_value(&self, register: &CPURegister) -> Option<u8> {
        match self.registers.get(register) {
            Some(reg)   => { Some(reg.as_u8()) },
            None        => { None }
        }
    }

    pub fn get_register_as_mut(&mut self, register: &CPURegister) -> Option<&mut ByteRegister> {
        self.registers.get_mut(register)
    }

    pub fn get_program_counter_value(&self) -> u16 {
        self.program_counter
    }

    pub fn set_program_counter(&mut self, new_value: u16) {
        self.program_counter = new_value;
    }

    pub fn inc_program_counter(&mut self) {
        self.set_program_counter(self.get_program_counter_value() + 2);
    }

    pub fn push_to_stack(&mut self, value: u16) -> () {
        self.stack.push(value);
    }

    pub fn get_stack_head(&self) -> u16 {
        self.stack.last()
    }

    pub fn resume_from_stack(&mut self) -> () {
        if self.stack.is_empty() {
            panic!("attempt to load from empty stack!");
            return;
        }
        let value = self.stack.pop().unwrap();
        self.set_program_counter(value as u16);
        // self.set_program_counter(value as u16);
    }

    pub fn get_delay_timer_value(&self) -> u8 {
        self.delay_timer
    }

    pub fn set_delay_timer(&mut self, new_value: u8) {
        self.delay_timer = new_value;
    }

    pub fn get_sound_timer_value(&self) -> u8 {
        self.sound_timer
    }

    pub fn set_sound_timer(&mut self, new_value: u8) {
        self.sound_timer = new_value;
    }

    pub fn dec_delay_timer(&mut self) {
        let value = self.get_delay_timer_value();
        if value >= 1 {
            self.set_delay_timer(value - 1);
        }
    }

    pub fn dec_sound_timer(&mut self) {
        let value = self.get_sound_timer_value();
        if value >= 1 {
            self.set_sound_timer(value - 1);
        }
    }

    pub fn get_i_register_value_as_u16(&self) -> u16 {
        return self.i_register.as_u16()
    }

    pub fn set_i_register_value(&mut self, value: u16) {
        self.i_register.set_value_as_u16(value);
    }

    pub fn add_to_i_register_value(&mut self, value: u16) {
        self.set_i_register_value(self.get_i_register_value_as_u16() + value);
    }

    pub fn register_iter(&self) -> Iter<CPURegister, ByteRegister> {
        self.registers.iter()
    }

    pub fn press_key(&mut self, key: Key) {
        self.keyboard.press_key(key);
    }

    pub fn release_key(&mut self, key: Key) {
        self.keyboard.release_key(key);
    }

    pub fn any_key_is_pressed(&self) -> bool {
        self.keyboard.any_key_is_pressed()
    }

    pub fn first_pressed_key(&self) -> Option<u8> {
        self.keyboard.first_pressed_key()
    }

    pub fn key_is_pressed(&self, key_value: u8) -> bool {
        self.keyboard.key_press_contains_value(key_value)
    }
}
