use piston::Key;
use std::collections::HashMap;

pub struct KeyMapping {
    mapping: HashMap<Key, u8>,
    keyboard: HashMap<u8, bool>
}

impl KeyMapping {
    pub fn new() -> KeyMapping {
        KeyMapping {
            mapping: KeyMapping::get_key_mapping(),
            keyboard: KeyMapping::get_keyboard_interface()
        }
    }

    fn get_keyboard_interface() -> HashMap<u8, bool> {
        let mut keyboard = HashMap::new();
        for key in 0x0..0xF {
            keyboard.insert(key, false);
        }

        keyboard
    }

    pub fn press_key(&mut self, key: Key) {
        if !self.mapping.contains_key(&key) {
            return;
        }

        let key_value = self.mapping.get(&key).unwrap();
        if !self.keyboard.contains_key(&key_value) {
            return;
        }

        if let Some(key_down) = self.keyboard.get_mut(&key_value) {
            *key_down = true;
        }
    }

    pub fn any_key_is_pressed(&self) -> bool {
        let mut key_pressed = false;
        for (key, value) in self.keyboard.iter() {
            if *value {
                key_pressed = true;
            }
        }

        key_pressed
    }

    pub fn first_pressed_key(&self) -> Option<u8> {
        for (key, value) in self.keyboard.iter() {
            if *value {
                return Some(*key);
            }
        }

        None
    }

    pub fn release_key(&mut self, key: Key) {
        if !self.mapping.contains_key(&key) {
            return;
        }

        let key_value = self.mapping.get(&key).unwrap();
        if !self.keyboard.contains_key(&key_value) {
            return;
        }

        if let Some(key_down) = self.keyboard.get_mut(&key_value) {
            *key_down = false;
        }
    }

    pub fn key_is_pressed(&self, key: Key) -> bool {
        if !self.mapping.contains_key(&key) {
            return false;
        }

        let key_value = self.mapping.get(&key).unwrap();
        if !self.keyboard.contains_key(&key_value) {
            return false;
        }

        *self.keyboard
            .get(&key_value)
            .unwrap()
    }

    pub fn key_press_contains_value(&self, desired_value: u8) -> bool {
        if let Some(key) = self.key_from_u8(desired_value) {
            return self.key_is_pressed(key);
        }

        false
    }

    fn key_from_u8(&self, desired_value: u8) -> Option<Key> {
        for (key, value) in self.mapping.iter() {
            if *value == desired_value {
                return Some(key.clone());
            }
        }

        None
    }

    fn get_key_mapping() -> HashMap<Key, u8> {
        
        let mut map =  HashMap::new();

        map.insert(Key::D1, 0x0);
        map.insert(Key::D2, 0x1);
        map.insert(Key::D3, 0x2);
        map.insert(Key::D4, 0x3);
        map.insert(Key::Q, 0x4);
        map.insert(Key::W, 0x5);
        map.insert(Key::E, 0x6);
        map.insert(Key::R, 0x7);
        map.insert(Key::A, 0x8);
        map.insert(Key::S, 0x9);
        map.insert(Key::D, 0xA);
        map.insert(Key::F, 0xB);
        map.insert(Key::Z, 0xC);
        map.insert(Key::X, 0xD);
        map.insert(Key::C, 0xE);
        map.insert(Key::V, 0xF);

        return map;
    }

    pub fn contains_key(&self, key: &Key) -> bool {
        self.mapping.contains_key(key)
    }

    pub fn map_key_to_value(&self, key: &Key) -> Option<&u8> {
        self.mapping.get(key)
    }
}