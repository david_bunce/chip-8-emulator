pub struct Stack {
    stack: Vec<u16>
}

impl Stack {
    pub fn new() -> Stack {
        Stack {
            stack: vec![]
        }
    }

    pub fn pop(&mut self) -> Option<u16> {
        self.stack.pop()
    }

    pub fn push(&mut self, word: u16) {
        self.stack.push(word)
    }

    pub fn is_empty(&self) -> bool {
        self.stack.is_empty()
    }

    pub fn last(&self) -> u16 {
        *self.stack.last().unwrap()
    }
}