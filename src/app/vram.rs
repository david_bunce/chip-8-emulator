use piston::window::*;

const WINDOW_WIDTH: usize = 64;
const WINDOW_HEIGHT: usize = 32;
const SCALE: usize = 4;

type Colour = [f32; 4];

pub struct VRAM {
    off_colour: Colour,
    on_colour: Colour,
    pixels: [[bool; WINDOW_WIDTH]; WINDOW_HEIGHT]
}

impl VRAM {
    pub fn new() -> VRAM {
        VRAM {
            off_colour: [0.0, 0.0, 0.0, 1.0],
            on_colour: [1.0, 1.0, 1.0, 1.0],
            pixels: [[false; WINDOW_WIDTH]; WINDOW_HEIGHT]
        }
    }

    pub fn render(&self) -> Vec<PixelRenderData> {
        let mut render_data: Vec<PixelRenderData> = vec![];
        for (row_index, row) in self.pixels.iter().enumerate() {
            for (column_index, pixel_value) in row.iter().enumerate() {
                let mut colour = self.off_colour;
                if *pixel_value {
                    colour = self.on_colour;
                }
                render_data.push(PixelRenderData::new(column_index as f64, row_index as f64, colour));
            }
        }

        return render_data;
    }

    pub fn print_sprite_row(&mut self, x_offset: u8, y_offset: u8, byte: u8) -> bool {
        for bit in 0..8 {
            let current_pixel_value = self.get_pixel_value(y_offset as usize, x_offset as usize + bit);
            let sprite_pixel_value = VRAM::bit_of_digit(byte, bit as u8);

            if x_offset as usize + bit > (VRAM::get_window_width() as usize) {
                return false;
            }

            if current_pixel_value && sprite_pixel_value {
                self.deactivate_pixel(y_offset as usize, x_offset as usize + bit);
                return true;
            }

            if !current_pixel_value && sprite_pixel_value {
                self.activate_pixel(y_offset as usize, x_offset as usize + bit);
            }
        }

        false
    }

    pub fn get_pixel_value(&self, row: usize, column: usize) -> bool {
        if row < (VRAM::get_window_height() as usize) && column < (VRAM::get_window_width() as usize) {
            return self.pixels[row][column];
        }

        false
    }

    fn deactivate_pixel(&mut self, row: usize, column: usize) {
        if row < (VRAM::get_window_height() as usize)  && column < (VRAM::get_window_width() as usize) {
            self.pixels[row][column] = false;
        }
    }

    fn activate_pixel(&mut self, row: usize, column: usize) {
        if row < (VRAM::get_window_height() as usize)  && column < (VRAM::get_window_width() as usize) {
            self.pixels[row][column] = true;
        }
    }

    pub fn clear_screen(&mut self) {
        self.pixels = [[false; WINDOW_WIDTH]; WINDOW_HEIGHT];
    }

    fn bit_of_digit(number: u8, digit: u8) -> bool {
        
        let bit_value: u8;
        match digit {
            0x00 => { bit_value = (number & 0x80) >> 7 },
            0x01 => { bit_value = (number & 0x40) >> 6 },
            0x02 => { bit_value = (number & 0x20) >> 5 },
            0x03 => { bit_value = (number & 0x10) >> 4 },
            0x04 => { bit_value = (number & 0x08) >> 3 },
            0x05 => { bit_value = (number & 0x04) >> 2 },
            0x06 => { bit_value = (number & 0x02) >> 1 },
            0x07 => { bit_value = number & 0x01 },
            _ => { return false }
        }

        return bit_value > 0;
    }

    pub fn get_window_width() -> u8 {
        WINDOW_WIDTH as u8
    }

    pub fn get_window_height() -> u8 {
        WINDOW_HEIGHT as u8
    }

    pub fn get_window_scale() -> usize {
        SCALE
    }
}

pub struct PixelRenderData {
    h_offset: f64,
    v_offset: f64,
    colour: [f32; 4],
    width: f64
}

impl PixelRenderData {
    pub fn new(h_offset: f64, v_offset: f64, colour: [f32; 4]) -> PixelRenderData {
        PixelRenderData {
            h_offset, 
            v_offset, 
            colour,
            width: 1.0
        }
    }

    pub fn get_h_offset(&self) -> f64 {
        self.h_offset
    }

    pub fn get_v_offset(&self) -> f64 {
        self.v_offset
    }

    pub fn get_colour(&self) -> [f32; 4] {
        self.colour
    }

    pub fn get_width(&self) -> f64 {
        self.width
    }
}