use crate::app::cpu::CPU;
use crate::app::instruction_executor::InstructionExecutor;
use crate::app::ram::RAM;
use crate::app::vram::VRAM;
use crate::app::font::Font;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent, ReleaseEvent, PressEvent, Button, Key};
use piston::window::WindowSettings;
use graphics::*;

use std::fs::*;
use std::time;
use std::thread::sleep;
use std::io::prelude::*;

const RAM_FONT_LOCATION: u8 = 0x50;

pub struct Chip8 {
    cpu: CPU,
    ram: RAM,
    font_data: Vec<u8>,
    start_location: u16,
    window: Window,
    graphics: GlGraphics,
    vram: VRAM
}

impl Chip8 {
    pub fn new() -> Chip8 {

        let opengl = OpenGL::V3_2;
        let mut window: Window = WindowSettings::new("Chip8", [VRAM::get_window_width() as u32 * VRAM::get_window_scale() as u32, VRAM::get_window_height() as u32 * VRAM::get_window_scale() as u32])
            .graphics_api(opengl)
            .exit_on_esc(true)
            .build()
            .unwrap();

        let mut glutin = GlGraphics::new(opengl);

        Chip8 {
            cpu: CPU::new(),
            ram: RAM::new(),
            font_data: Chip8::get_font(),
            start_location: 0x200,
            window: window,
            graphics: glutin,
            vram: VRAM::new()
        }
    }

    pub fn get_font() -> Vec<u8>
    {
        Font::get_font_data()
    }

    pub fn set_ram_location(&mut self, value: u8, offset: usize) -> Result<(), &str> {
        self.ram.save_byte(value, offset)
    }

    pub fn print_ram(&self) {
        for no in 0x200..0x300 {
            self.ram.print_offset(no as usize);
        }
    }

    pub fn load_font_into_ram(&mut self) {
        let ram_offset: usize = RAM_FONT_LOCATION as usize;
        for byte in (*self.font_data).into_iter() {
            match self.ram.save_byte(*byte, ram_offset) {
                Ok(val)     => {},
                _           => { panic!("Storing font failed") }
            }
        }
    }

    pub fn open_game_file(&mut self, path: &String) {
        let f = File::open(path);

        if let Ok(file) = f {
            self.load_game_into_ram(file);
        } else {
            panic!("Sorry, game could not be opened.");
        }
    }

    pub fn load_game_into_ram(&mut self, file: File) {
        let mut location = 0x200;
        for b in file.bytes() {
            if let Ok(byte_value) = b {
                self.set_ram_location(byte_value, location as usize);
                location += 1;
            }
        }
    }

    pub fn launch_game(&mut self) {

        let starting_location = 0x200;
        self.cpu.set_program_counter(starting_location);
        
        let mut events = Events::new(EventSettings::new());
        while let Some(e) = events.next(&mut self.window) {

            self.run_execution_loop();
            let pixels = self.vram.render();

            if let Some(Button::Keyboard(key)) = e.press_args() {
                self.cpu.press_key(key);
            }

            if let Some(Button::Keyboard(key)) = e.release_args() {
                self.cpu.release_key(key);
            }

            if let Some(args) = e.render_args() {
                self.graphics.draw(args.viewport(), |c, gl| {

                    clear([0.0, 0.0, 0.0, 1.0], gl);

                    for px in pixels {

                        let transform = c
                        .transform
                        .trans(px.get_h_offset() * VRAM::get_window_scale() as f64, px.get_v_offset() * VRAM::get_window_scale() as f64);
        
                        let square = rectangle::square(0.0, 0.0, px.get_width() * VRAM::get_window_scale() as f64);

                        rectangle(px.get_colour(), square, transform, gl);
                    }
                });
            }

            if let Some(args) = e.update_args() {
                
            }
        }
    }

    fn run_execution_loop(&mut self) {
        println!("--    --");
        println!("Program Counter: {:#X?}", self.cpu.get_program_counter_value());
        println!("Opcode : {:#X?}", self.ram.read_word(self.cpu.get_program_counter_value() as usize));
        
        let opcode = self.ram.read_word(self.cpu.get_program_counter_value() as usize);
        if !InstructionExecutor::execute(&mut self.cpu, &mut self.ram, opcode, &mut self.vram) {
            return;
        }
        
        self.cpu.inc_program_counter();

        self.cpu.dec_delay_timer();
        self.cpu.dec_sound_timer();
    }
}