#[derive(Debug)]
pub enum OpcodeInstruction {
    NoOp,
    Store,
    Add,
    Copy,
    BitwiseOr,
    BitwiseAnd,
    BitwiseXOr,
    AddWithFlag,
    SubtractWithFlag,
    SubtractReverseWithFlag,
    Rightshift,
    Leftshift,
    StoreRandomNumber,
    Jump,
    JumpWithOffset,
    CallSubroutine,
    ReturnFromSubroutine,
    ExecuteInstruction,
    SkipIfRegisterEqualsValue,
    SkipIfRegisterEqualsRegister,
    SkipIfRegisterNotEqualsValue,
    SkipIfRegisterNotEqualsRegister,
    SetDelayTimerToRegister,
    SetRegisterToDelayTimer,
    SetSoundTimerToRegister,
    HaltForKeyPress,
    SkipIfKeyPressEqualsRegister,
    SkipIfKeyPressNotEqualsRegister,
    StoreInIRegister,
    AddToIRegister,
    DrawSpriteData,
    ClearScreen,
    SetIRegisterToSpriteAddress,
    DecimalEncode,
    StoreRegisterValuesInRAM,
    LoadRegistersFromRAM,
}

impl OpcodeInstruction {
    pub fn from_opcode(opcode: u16) -> OpcodeInstruction {
        
        let first = ((opcode & 0xF000) >> 12) as u8;
        let second = ((opcode & 0x0F00) >> 8) as u8;
        let third = ((opcode & 0x00F0) >> 4) as u8;
        let fourth = (opcode & 0x000F) as u8;

        println!("1: {:#X?}", first);
        println!("2: {:#X?}", second);
        println!("3: {:#X?}", third);
        println!("4: {:#X?}", fourth);

        if first == 0 {
            if second == 0 && third == 0xE && fourth == 0xE {
                return OpcodeInstruction::ReturnFromSubroutine;
            }

            if second == 0 && third == 0xE && fourth == 0 {
                return OpcodeInstruction::ClearScreen;
            }

            return OpcodeInstruction::ExecuteInstruction;
        }

        if first == 1 {
            return OpcodeInstruction::Jump;
        }

        if first == 2 {
            return OpcodeInstruction::CallSubroutine;
        }

        if first == 3 {
            return OpcodeInstruction::SkipIfRegisterEqualsValue;
        }

        if first == 4 {
            return OpcodeInstruction::SkipIfRegisterNotEqualsValue;
        }

        if first == 5 && fourth == 0 {
            return OpcodeInstruction::SkipIfRegisterEqualsRegister;
        }

        if first == 6 {
            return OpcodeInstruction::Store;
        }

        if first == 7 {
            return OpcodeInstruction::Add;
        }

        if first == 8 {
            if fourth == 0 {
                return OpcodeInstruction::Copy;
            }

            if fourth == 1 {
                return OpcodeInstruction::BitwiseOr;
            }

            if fourth == 2 {
                return OpcodeInstruction::BitwiseAnd;
            }

            if fourth == 3 {
                return OpcodeInstruction::BitwiseXOr;
            }

            if fourth == 4 {
                return OpcodeInstruction::AddWithFlag;
            }

            if fourth == 5 {
                return OpcodeInstruction::SubtractWithFlag;
            }

            if fourth == 6 {
                return OpcodeInstruction::Rightshift;
            }

            if fourth == 7 {
                return OpcodeInstruction::SubtractReverseWithFlag;
            }

            if fourth == 0xE {
                return OpcodeInstruction::Leftshift;
            }
        }

        if first == 9 && fourth == 0 {
            return OpcodeInstruction::SkipIfRegisterNotEqualsRegister;
        }

        if first == 0xA {
            return OpcodeInstruction::StoreInIRegister;
        }

        if first == 0xB {
            return OpcodeInstruction::JumpWithOffset;
        }

        if first == 0xC {
            return OpcodeInstruction::StoreRandomNumber;
        }

        if first == 0xD {
            return OpcodeInstruction::DrawSpriteData;
        }

        if first == 0xE {
            if third == 0x9 && fourth == 0xE {
                return OpcodeInstruction::SkipIfKeyPressEqualsRegister;
            }

            if third == 0xA && fourth == 1 {
                return OpcodeInstruction::SkipIfKeyPressNotEqualsRegister;
            }
        }

        if first == 0xF {
            if third == 0 && fourth == 7 {
                return OpcodeInstruction::SetRegisterToDelayTimer;
            }

            if third == 0 && fourth == 0xA {
                return OpcodeInstruction::HaltForKeyPress;
            }

            if third == 1 && fourth == 5 {
                return OpcodeInstruction::SetDelayTimerToRegister;
            }

            if third == 1 && fourth == 8 {
                return OpcodeInstruction::SetSoundTimerToRegister;
            }

            if third == 1 && fourth == 0xE {
                return OpcodeInstruction::AddToIRegister;
            }

            if third == 2 && fourth == 9 {
                return OpcodeInstruction::SetIRegisterToSpriteAddress;
            }

            if third == 3 && fourth == 3 {
                return OpcodeInstruction::DecimalEncode;
            }

            if third == 5 && fourth == 5 {
                return OpcodeInstruction::StoreRegisterValuesInRAM;
            }

            if third == 6 && fourth == 5 {
                return OpcodeInstruction::LoadRegistersFromRAM;
            }
        }

        return OpcodeInstruction::NoOp;
    }

    pub fn opcoode_is_noop(opcode: u16) -> bool {
        if let OpcodeInstruction::NoOp = OpcodeInstruction::from_opcode(opcode) {
            return true;
        }

        false
    }
}