
use crate::app::ram::RAM;
use crate::app::vram::VRAM;
use crate::app::cpu::CPU;
use crate::app::instruction::*;
use crate::app::opcode::OpcodeInstruction;

use piston::Button;

pub struct InstructionExecutor {

}

impl InstructionExecutor {
    pub fn execute(cpu: &mut CPU, ram: &mut RAM, opcode: u16, vram: &mut VRAM) -> bool {
        let opcode_instruction = OpcodeInstruction::from_opcode(opcode);
        let factory = InstructionFactory::new(opcode as u16);
        println!("Instruction: {:?}", opcode_instruction);
         match opcode_instruction {
            OpcodeInstruction::Store                            => { return factory.store_in_register().execute(cpu, ram, vram); },
            OpcodeInstruction::Add                              => { return factory.add_byte().execute(cpu, ram, vram); },
            OpcodeInstruction::Copy                             => { return factory.copy().execute(cpu, ram, vram); },
            OpcodeInstruction::BitwiseOr                        => { return factory.bitwise_or().execute(cpu, ram, vram); },
            OpcodeInstruction::BitwiseAnd                       => { return factory.bitwise_and().execute(cpu, ram, vram); },
            OpcodeInstruction::BitwiseXOr                       => { return factory.bitwise_xor().execute(cpu, ram, vram); },
            OpcodeInstruction::AddWithFlag                      => { return factory.add_with_flag().execute(cpu, ram, vram); },
            OpcodeInstruction::SubtractWithFlag                 => { return factory.subtract_with_flag().execute(cpu, ram, vram); },
            OpcodeInstruction::Rightshift                       => { return factory.right_shift().execute(cpu, ram, vram); },
            OpcodeInstruction::SubtractReverseWithFlag          => { return factory.subtract_reverse_with_flag().execute(cpu, ram, vram); },
            OpcodeInstruction::Leftshift                        => { return factory.left_shift().execute(cpu, ram, vram); },
            OpcodeInstruction::StoreRandomNumber                => { return factory.random_number().execute(cpu, ram, vram); },
            OpcodeInstruction::Jump                             => { return factory.jump().execute(cpu, ram, vram); },
            OpcodeInstruction::JumpWithOffset                   => { return factory.jump_with_offset().execute(cpu, ram, vram); },
            OpcodeInstruction::CallSubroutine                   => { return factory.call_subroutine().execute(cpu, ram, vram); },
            OpcodeInstruction::ReturnFromSubroutine             => { return factory.return_from_subroutine().execute(cpu, ram, vram); },
            OpcodeInstruction::ExecuteInstruction               => { return factory.execute_instruction().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfRegisterEqualsValue        => { return factory.skip_if_register_equals_value().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfRegisterEqualsRegister     => { return factory.skip_if_register_equals_register().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfRegisterNotEqualsValue     => { return factory.skip_if_register_not_equals_value().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfRegisterNotEqualsRegister  => { return factory.skip_if_register_not_equals_register().execute(cpu, ram, vram); },
            OpcodeInstruction::SetDelayTimerToRegister          => { return factory.set_delay_timer_to_register().execute(cpu, ram, vram); },
            OpcodeInstruction::SetRegisterToDelayTimer          => { return factory.set_register_to_delay_timer().execute(cpu, ram, vram); },
            OpcodeInstruction::SetSoundTimerToRegister          => { return factory.set_sound_timer_to_register().execute(cpu, ram, vram); },
            OpcodeInstruction::HaltForKeyPress                  => { return factory.halt_for_key_press().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfKeyPressEqualsRegister     => { return factory.skip_if_key_press_equals_register().execute(cpu, ram, vram); },
            OpcodeInstruction::SkipIfKeyPressNotEqualsRegister  => { return factory.skip_if_key_press_not_equals_register().execute(cpu, ram, vram); },
            OpcodeInstruction::StoreInIRegister                 => { return factory.store_in_i_register().execute(cpu, ram, vram); },
            OpcodeInstruction::AddToIRegister                   => { return factory.add_to_i_register().execute(cpu, ram, vram); },
            OpcodeInstruction::DrawSpriteData                   => { return factory.draw_sprite_data().execute(cpu, ram, vram); },
            OpcodeInstruction::ClearScreen                      => { return factory.clear_screen().execute(cpu, ram, vram); },
            OpcodeInstruction::SetIRegisterToSpriteAddress      => { return factory.set_i_register_to_sprite_address().execute(cpu, ram, vram); },
            OpcodeInstruction::DecimalEncode                    => { return factory.decimal_encode().execute(cpu, ram, vram); },
            OpcodeInstruction::StoreRegisterValuesInRAM         => { return factory.store_register_values_in_ram().execute(cpu, ram, vram); },
            OpcodeInstruction::LoadRegistersFromRAM             => { return factory.load_registers_from_ram().execute(cpu, ram, vram); },
            _                                                   => { return factory.noop().execute(cpu, ram, vram); },
        };

    }
}