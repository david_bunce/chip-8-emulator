use rand::prelude::*;
use crate::app::ram::RAM;
use crate::app::vram::VRAM;
use crate::app::cpu::CPU;
use crate::app::register::CPURegister;
use crate::app::opcode::OpcodeInstruction;
use crate::app::keymapping::KeyMapping;
use crate::app::instruction_executor::InstructionExecutor;

use piston::Button;

pub struct InstructionFactory {
    first: u8,
    second: u8,
    third: u8,
    fourth: u8
}

impl InstructionFactory {

    pub fn new(opcode: u16) -> InstructionFactory {
        InstructionFactory {
            first: ((opcode & 0xF000) >> 12) as u8,
            second: ((opcode & 0x0F00) >> 8) as u8,
            third: ((opcode & 0x00F0) >> 4) as u8,
            fourth: (opcode & 0x000F) as u8
        }
    }

    pub fn store_in_register(&self) -> StoreByte {
        return StoreByte::new(CPURegister::from_u8(self.second), self.third, self.fourth);
    }

    pub fn add_byte(&self) -> AddByte {
        return AddByte::new(CPURegister::from_u8(self.second), self.third, self.fourth);
    }

    pub fn copy(&self) -> Copy {
        return Copy::new(CPURegister::from_u8(self.third), CPURegister::from_u8(self.second));
    }

    pub fn bitwise_or(&self) -> BitwiseOr {
        return BitwiseOr::new(CPURegister::from_u8(self.third), CPURegister::from_u8(self.second));
    }

    pub fn bitwise_and(&self) -> BitwiseAnd {
        return BitwiseAnd::new(CPURegister::from_u8(self.third), CPURegister::from_u8(self.second));
    }

    pub fn bitwise_xor(&self) -> BitwiseXor {
        return BitwiseXor::new(CPURegister::from_u8(self.third), CPURegister::from_u8(self.second));
    }

    pub fn add_with_flag(&self) -> AddWithFlag {
        return AddWithFlag::new(CPURegister::from_u8(self.third), CPURegister::from_u8(self.second), CPURegister::from_u8(15));
    }

    pub fn subtract_with_flag(&self) -> SubtractWithFlag {
        let minuend = CPURegister::from_u8(self.second);
        let subtrahend = CPURegister::from_u8(self.third);
        
        return SubtractWithFlag::new(minuend, subtrahend);
    }

    pub fn right_shift(&self) -> RightShift {
        let source_register = CPURegister::from_u8(self.third);
        let destination_register = CPURegister::from_u8(self.second);

        return RightShift::new(source_register, destination_register);
    }

    pub fn subtract_reverse_with_flag(&self) -> SubtractWithFlag {
        let minuend = CPURegister::from_u8(self.third);
        let subtrahend = CPURegister::from_u8(self.second);
        
        return SubtractWithFlag::new(minuend, subtrahend);
    }

    pub fn left_shift(&self) -> LeftShift {
        let source_register = CPURegister::from_u8(self.second);
        let destination_register = CPURegister::from_u8(self.second);
        let flags = CPURegister::from_u8(15);

        return LeftShift::new(source_register, destination_register, flags);
    }

    pub fn random_number(&self) -> RandomNumber {
        let upper = self.third << 4;
        let byte_mask = upper & self.fourth;
        let destination = CPURegister::from_u8(self.second);
        return RandomNumber::new(destination, byte_mask);
    }

    pub fn jump(&self) -> Jump {
        let second_hundreds = (self.second as u16) * 0x100;
        let third_tens = (self.third as u16) * 0x010;
        let fourth_units = (self.fourth as u16) * 0x001;

        let offset = second_hundreds + third_tens + fourth_units;
        return Jump::new(offset);
    }

    pub fn jump_with_offset(&self) -> JumpWithOffset {
        let second_hundreds = (self.second as u16) * 0x100;
        let third_tens = (self.third as u16) * 0x010;
        let fourth_units = (self.fourth as u16) * 0x001;

        let offset = second_hundreds + third_tens + fourth_units;
        return JumpWithOffset::new(offset, CPURegister::from_u8(0));
    }

    pub fn call_subroutine(&self) -> CallSubroutine {
        let second_hundreds = (self.second as u16) * 0x100;
        let third_tens = (self.third as u16) * 0x010;
        let fourth_units = (self.fourth as u16) * 0x001;

        let offset = second_hundreds + third_tens + fourth_units;
        return CallSubroutine::new(offset);
    }

    pub fn return_from_subroutine(&self) -> ReturnFromSubroutine {
        return ReturnFromSubroutine::new();
    }

    pub fn execute_instruction(&self) -> ExecuteInstructionFromRAM {
        let second_hundreds = (self.second as u16) * 0x100;
        let third_tens = (self.third as u16) * 0x010;
        let fourth_units = (self.fourth as u16) * 0x001;

        let offset = second_hundreds + third_tens + fourth_units;
        return ExecuteInstructionFromRAM::new(offset);
    }

    pub fn skip_if_register_equals_value(&self) -> SkipIfRegisterEqualsValue {
        let register = CPURegister::from_u8(self.second);
        let value = (self.third << 4) + self.fourth;
        return SkipIfRegisterEqualsValue::new(register, value);
    }

    pub fn skip_if_register_equals_register(&self) -> SkipIfRegisterEqualsRegister {
        let source_register = CPURegister::from_u8(self.second);
        let destination_register = CPURegister::from_u8(self.third);
        return SkipIfRegisterEqualsRegister::new(source_register, destination_register);
    }

    pub fn skip_if_register_not_equals_value(&self) -> SkipIfRegisterNotEqualsValue {
        let register = CPURegister::from_u8(self.second);
        let value = (self.third << 4) + self.fourth;
        return SkipIfRegisterNotEqualsValue::new(register, value);
    }

    pub fn skip_if_register_not_equals_register(&self) -> SkipIfRegisterNotEqualsRegister {
        let source_register = CPURegister::from_u8(self.second);
        let destination_register = CPURegister::from_u8(self.third);
        return SkipIfRegisterNotEqualsRegister::new(source_register, destination_register);
    }

    pub fn set_delay_timer_to_register(&self) -> SetDelayTimerToRegister {
        return SetDelayTimerToRegister::new(CPURegister::from_u8(self.second));
    }

    pub fn set_register_to_delay_timer(&self) -> SetRegisterToDelayTimer {
        return SetRegisterToDelayTimer::new(CPURegister::from_u8(self.second));
    }

    pub fn set_sound_timer_to_register(&self) -> SetSoundTimerToRegister {
        return SetSoundTimerToRegister::new(CPURegister::from_u8(self.second));
    }

    pub fn halt_for_key_press(&self) -> HaltForKeyPress {
        return HaltForKeyPress::new(CPURegister::from_u8(self.second));
    }

    pub fn skip_if_key_press_equals_register(&self) -> SkipIfKeyPressEqualsRegister {
        return SkipIfKeyPressEqualsRegister::new(CPURegister::from_u8(self.second));
    }

    pub fn skip_if_key_press_not_equals_register(&self) -> SkipIfKeyPressNotEqualsRegister {
        return SkipIfKeyPressNotEqualsRegister::new(CPURegister::from_u8(self.second));
    }

    pub fn store_in_i_register(&self) -> StoreInIRegister {
        let second_hundreds = (self.second as u16) * 0x100;
        let third_tens = (self.third as u16) * 0x010;
        let fourth_units = (self.fourth as u16) * 0x001;

        return StoreInIRegister::new(second_hundreds + third_tens + fourth_units);
    }

    pub fn add_to_i_register(&self) -> AddToIRegister {
        return AddToIRegister::new(CPURegister::from_u8(self.second));
    }

    pub fn draw_sprite_data(&self) -> DrawSpriteData {
        return DrawSpriteData::new(CPURegister::from_u8(self.second), CPURegister::from_u8(self.third), self.fourth as u8);
    }

    pub fn clear_screen(&self) -> ClearScreen {
        return ClearScreen::new();
    }

    pub fn set_i_register_to_sprite_address(&self) -> SetIRegisterToSpriteAddress {
        return SetIRegisterToSpriteAddress::new(CPURegister::from_u8(self.second));
    }

    pub fn decimal_encode(&self) -> DecimalEncode {
        return DecimalEncode::new(CPURegister::from_u8(self.second));
    }

    pub fn store_register_values_in_ram(&self) -> StoreRegisterValuesInRAM {
        return StoreRegisterValuesInRAM::new(CPURegister::from_u8(self.second));
    }

    pub fn load_registers_from_ram(&self) -> LoadRegistersFromRAM {
        return LoadRegistersFromRAM::new(CPURegister::from_u8(self.second));
    }

    pub fn noop(&self) -> NoOp {
        return NoOp::new();
    }
}

pub trait Executable {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, vram: &mut VRAM) -> bool;
    fn rollback(&mut self, cpu: &mut CPU, ram: &mut RAM);
    fn record_value(&mut self, cpu: &mut CPU, ram: &mut RAM);
    fn is_noop(&self) -> bool {
        return false;
    }
}

pub struct NoOp {

}

impl NoOp {
    fn new() -> NoOp {
        NoOp {

        }
    }
}

impl Executable for NoOp {
    fn execute(&mut self, _cpu: &mut CPU, _ram: &mut RAM, _vram: &mut VRAM) -> bool {

        return true;
    }

    fn rollback(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {

    }

    fn record_value(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {

    }

    fn is_noop(&self) -> bool {
        return true;
    }
}

pub struct StoreByte {
    value: u8,
    register: CPURegister,
    previous_value: u8
}

impl StoreByte {
    fn new(register: CPURegister, digit_1: u8, digit_2: u8) -> StoreByte {
        StoreByte {
            value: (digit_1 * 0x10) + digit_2,
            register: register,
            previous_value: 0
        }
    }
}

impl Executable for StoreByte {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        cpu.get_register_as_mut(&self.register)
            .unwrap()
            .set_value_as_u8(self.value);
            
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.register).unwrap();
        register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod store_test {
    use super::*;

    #[test]
    fn store_byte_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for register_u8 in 0x0..0xF {
            let register: CPURegister = CPURegister::from_u8(register_u8);

            for first in 0x0..=0xF {
                for second in 0x0..=0xF {
                    StoreByte::new(register, first, second).execute(&mut cpu, &mut ram, &mut vram);
                    assert_eq!((first * 0x10) + second, cpu.get_register_value(&register).unwrap());

                }
            }
        }
    }
}

pub struct AddByte {
    value: u8,
    register: CPURegister,
    previous_value: u8
}

impl AddByte {
    fn new(register: CPURegister, digit_1: u8, digit_2: u8) -> AddByte {
        AddByte {
            value: (digit_1 * 0x10) + digit_2,
            register: register,
            previous_value: 0
        }
    }
}

impl Executable for AddByte {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let register = cpu.get_register_as_mut(&self.register).unwrap();
        register.add(self.value);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.register).unwrap();
        register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod add_test {
    use super::*;

    #[test]
    fn  add_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for register_u8 in 0x0..0xF {
            let register: CPURegister = CPURegister::from_u8(register_u8);

            for first in 0x0..=0xF {
                for second in 0x0..=0xF {
                    for original_register_value in 0x00..=0xFF as u8 {
                        let (final_register_value, _did_overflow) = original_register_value.overflowing_add((first * 0x10) + second);
                        
                        cpu.get_register_as_mut(&register)
                            .unwrap()
                            .set_value_as_u8(original_register_value);

                        AddByte::new(register, first, second).execute(&mut cpu, &mut ram, &mut vram);
                        assert_eq!(final_register_value, cpu.get_register_value(&register).unwrap());
                    }
                }
            }
        }
    }
}

pub struct Copy {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u8
}

impl Copy {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> Copy {
        Copy {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0
        }
    }
}

impl Executable for Copy {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(source_register_value);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod copy_test {
    use super::*;

    #[test]
    fn copy_test()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for source_register in 0x0..=0xF {
            for destination_register in 0x0..=0xF {
                for source_register_value in 0x00..=0xFF {
                    if source_register == destination_register {
                        continue;
                    }

                    let source_register = CPURegister::from_u8(source_register);
                    let destination_register = CPURegister::from_u8(destination_register);

                    cpu.get_register_as_mut(&source_register)
                        .unwrap()
                        .set_value_as_u8(source_register_value);

                    cpu.get_register_as_mut(&destination_register)
                        .unwrap()
                        .set_value_as_u8(0);

                    Copy::new(source_register, destination_register).execute(&mut cpu, &mut ram, &mut vram);
                    assert_eq!(source_register_value, cpu.get_register_value(&destination_register).unwrap());
                }
            }
        }
    }
}

pub struct BitwiseOr {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u8
}

impl BitwiseOr {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> BitwiseOr {
        BitwiseOr {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0
        }
    }
}

impl Executable for BitwiseOr {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(source_register_value | destination_register.as_u8());
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod test_bitwise_or {
    use super::*;

    #[test]
    fn bitwise_or_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for source_register in 0x00..=0x0F {
            let source = CPURegister::from_u8(source_register);
            for destination_register in 0x00..0x0F {
                if source_register == destination_register {
                    continue;
                }
                let destination = CPURegister::from_u8(destination_register);
                for source_value in 0x00..=0xFF {
                    for destination_value in 0x00..=0xFF {
                        cpu.get_register_as_mut(&source)
                            .unwrap()
                            .set_value_as_u8(source_value);

                        cpu.get_register_as_mut(&destination)
                            .unwrap()
                            .set_value_as_u8(destination_value);

                        BitwiseOr::new(source, destination).execute(&mut cpu, &mut ram, &mut vram);
                        assert_eq!(source_value | destination_value, cpu.get_register_value(&destination).unwrap());
                    }
                }
            }
        }
    }
}

pub struct BitwiseAnd {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u8
}

impl BitwiseAnd {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> BitwiseAnd {
        BitwiseAnd {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0
        }
    }
}

impl Executable for BitwiseAnd {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(source_register_value & destination_register.as_u8());
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod test_bitwise_and {
    use super::*;

    #[test]
    fn bitwise_and_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for source_register in 0x00..=0x0F {
            let source = CPURegister::from_u8(source_register);
            for destination_register in 0x00..0x0F {
                if source_register == destination_register {
                    continue;
                }
                let destination = CPURegister::from_u8(destination_register);
                for source_value in 0x00..=0xFF {
                    for destination_value in 0x00..=0xFF {
                        cpu.get_register_as_mut(&source)
                            .unwrap()
                            .set_value_as_u8(source_value);
            
                        cpu.get_register_as_mut(&destination)
                            .unwrap()
                            .set_value_as_u8(destination_value);
            
                        BitwiseAnd::new(source, destination).execute(&mut cpu, &mut ram, &mut vram);
                        assert_eq!(source_value & destination_value, cpu.get_register_value(&destination).unwrap());
                    
                    }
                }
            }
        }
    }
}

pub struct BitwiseXor {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u8
}

impl BitwiseXor {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> BitwiseXor {
        BitwiseXor {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0
        }
    }
}

impl Executable for BitwiseXor {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(source_register_value ^ destination_register.as_u8());
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod test_bitwise_xor {
    use super::*;

    #[test]
    fn bitwise_xor_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        for source_register in 0x00..=0x0F {
            let source = CPURegister::from_u8(source_register);
            for destination_register in 0x00..0x0F {
                if source_register == destination_register {
                    continue;
                }
                let destination = CPURegister::from_u8(destination_register);
                for source_value in 0x00..=0xFF {
                    for destination_value in 0x00..=0xFF {
                        cpu.get_register_as_mut(&source)
                            .unwrap()
                            .set_value_as_u8(source_value);

                        cpu.get_register_as_mut(&destination)
                            .unwrap()
                            .set_value_as_u8(destination_value);

                        BitwiseXor::new(source, destination).execute(&mut cpu, &mut ram, &mut vram);
                        assert_eq!(source_value ^ destination_value, cpu.get_register_value(&destination).unwrap());
                    }
                }
            }
        }
    }
}

pub struct AddWithFlag {
    source_register: CPURegister,
    destination_register: CPURegister,
    flags_register: CPURegister,
    previous_value: u8,
    previous_flags_value: u8
}

impl AddWithFlag {
    fn new(source_register: CPURegister, destination_register: CPURegister, flags_register: CPURegister) -> AddWithFlag {
        AddWithFlag {
            source_register: source_register,
            destination_register: destination_register,
            flags_register: flags_register,
            previous_value: 0,
            previous_flags_value: 0
        }
    }
}

impl Executable for AddWithFlag {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        let (_new_value, did_overflow) = destination_register.add(source_register_value);

        if did_overflow {
            let flags_register = cpu.get_register_as_mut(&self.flags_register).unwrap();
            flags_register.set_value_as_u8(1);
        }
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        register.set_value_as_u8(self.previous_value);

        let flags_register = cpu.get_register_as_mut(&self.flags_register).unwrap();
        flags_register.set_value_as_u8(self.previous_flags_value)
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();

        self.previous_flags_value = cpu.get_register_as_mut(&self.flags_register)
            .unwrap()
            .as_u8();
    }
}

#[cfg(test)]
mod add_with_flag_test {
    use super::*;

    #[test]
    fn add_with_flag_executes()
    {
        let mut cpu = CPU::new();
        let mut ram = RAM::new();
        let mut vram = VRAM::new();

        let flags_register = CPURegister::from_u8(0x0F);
        for source_register in 0x00..0x0F {
            let source = CPURegister::from_u8(source_register);
            for destination_register in 0x00..0x0F {
                if source_register == destination_register {
                    continue;
                }
                let destination = CPURegister::from_u8(destination_register);
                for source_value in 0x00..=0xFF as u8 {
                    cpu.get_register_as_mut(&source)
                        .unwrap()
                        .set_value_as_u8(source_value);

                    for destination_value in 0x00..=0xFF {
                        cpu.get_register_as_mut(&destination)
                            .unwrap()
                            .set_value_as_u8(destination_value);

                        cpu.get_register_as_mut(&flags_register)
                            .unwrap()
                            .set_value_as_u8(0x02);

                        AddWithFlag::new(source, destination, flags_register).execute(&mut cpu, &mut ram, &mut vram);
                        let (result, did_overflow) = source_value.overflowing_add(destination_value);
                        
                        assert_eq!(result, cpu.get_register_value(&destination).unwrap());
                        assert_eq!(did_overflow, cpu.get_register_value(&flags_register).unwrap() == 0x01 as u8);
                    }
                }
            }
        }
    }
}

pub struct SubtractWithFlag {
    minuend_register: CPURegister,
    subtrahend_register: CPURegister,
    previous_value: u8,
    previous_flags_value: u8
}

impl SubtractWithFlag {
    fn new(minuend_register: CPURegister, subtrahend_register: CPURegister) -> SubtractWithFlag {
        SubtractWithFlag {
            minuend_register: minuend_register,
            subtrahend_register: subtrahend_register,
            previous_value: 0,
            previous_flags_value: 0
        }
    }
}

impl Executable for SubtractWithFlag {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let subtrahend_register_value = cpu.get_register_value(&self.subtrahend_register).unwrap();
        let minuend_register = cpu.get_register_as_mut(&self.minuend_register).unwrap();

        let (new_value, underflow) = minuend_register.subtract(subtrahend_register_value);

        cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .set_value_as_u8((!underflow) as u8);

        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.minuend_register).unwrap();
        register.set_value_as_u8(self.previous_value);

        let flags_register = cpu.get_register_as_mut(&CPURegister::from_u8(0xF)).unwrap();
        flags_register.set_value_as_u8(self.previous_flags_value)
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.minuend_register)
            .unwrap()
            .as_u8();

        self.previous_flags_value = cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .as_u8();
    }
}

pub struct RightShift {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u8,
    previous_flags_value: u8
}

impl RightShift {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> RightShift {
        RightShift {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0,
            previous_flags_value: 0
        }
    }
}

impl Executable for RightShift {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let carry_right = (source_register_value & 0x01) as u8;

        cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .set_value_as_u8(source_register_value >> 1);

        cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .set_value_as_u8(carry_right);

        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .set_value_as_u8(self.previous_value);

        cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .set_value_as_u8(self.previous_flags_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();

        self.previous_flags_value = cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .as_u8();
    }
}

pub struct LeftShift {
    source_register: CPURegister,
    destination_register: CPURegister,
    flags_register: CPURegister,
    previous_value: u8,
    previous_flags_value: u8
}

impl LeftShift {
    fn new(source_register: CPURegister, destination_register: CPURegister, flags_register: CPURegister) -> LeftShift {
        LeftShift {
            source_register: source_register,
            destination_register: destination_register,
            flags_register: flags_register,
            previous_value: 0,
            previous_flags_value: 0
        }
    }
}

impl Executable for LeftShift {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(source_register_value << 1);

        let flags_register = cpu.get_register_as_mut(&self.flags_register).unwrap();
        flags_register.set_value_as_u8(&self.previous_value & 0x80 as u8);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        register.set_value_as_u8(self.previous_value);

        let flags_register = cpu.get_register_as_mut(&self.flags_register).unwrap();
        flags_register.set_value_as_u8(self.previous_flags_value)
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();

        self.previous_flags_value = cpu.get_register_as_mut(&self.flags_register)
            .unwrap()
            .as_u8();
    }
}

pub struct RandomNumber
{
    destination_register: CPURegister,
    byte_mask: u8,
    previous_value: u8
}

impl RandomNumber {
    pub fn new(destination_register: CPURegister, byte_mask: u8) -> RandomNumber {
        RandomNumber {
            destination_register: destination_register,
            byte_mask: byte_mask,
            previous_value: 0
        }
    }
}

impl Executable for RandomNumber {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let random_number: u8 = random();
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(random_number & self.byte_mask);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let destination_register = cpu.get_register_as_mut(&self.destination_register).unwrap();
        destination_register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_as_mut(&self.destination_register)
            .unwrap()
            .as_u8();
    }
}

pub struct Jump {
    ram_offset: u16,
    previous_value: u16
}

impl Jump {
    fn new(ram_offset: u16) -> Jump {
        Jump {
            ram_offset,
            previous_value: 0 as u16
        }
    }

    fn destination_contains_valid_instruction(&self, instruction_value: u16) -> bool {
        !OpcodeInstruction::opcoode_is_noop(instruction_value)
    }
}

impl Executable for Jump {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        let instruction_value = ram.read_word(self.ram_offset as usize);
        if !self.destination_contains_valid_instruction(instruction_value) {
            panic!("Attempt to jump to location with invalid instruction");
        }

        cpu.set_program_counter(self.ram_offset - 2);
        // cpu.set_program_counter(self.ram_offset);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_program_counter(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct JumpWithOffset {
    ram_offset: u16,
    offset_register: CPURegister,
    previous_value: u16
}

impl JumpWithOffset {
    fn new(ram_offset: u16, offset_register: CPURegister) -> JumpWithOffset {
        JumpWithOffset {
            ram_offset,
            offset_register,
            previous_value: 0 as u16
        }
    }

    fn destination_contains_valid_instruction(&self, instruction_value: u16) -> bool {
        OpcodeInstruction::opcoode_is_noop(instruction_value)
    }
}

impl Executable for JumpWithOffset {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        let register_value = cpu.get_register_value(&self.offset_register).unwrap();
        let instruction_value = ram.read_word(self.ram_offset as usize + register_value as usize);
        
        if !self.destination_contains_valid_instruction(instruction_value) {
            return true;
        }

        cpu.set_program_counter((self.ram_offset as u16 + register_value as u16) - 2);
        // cpu.set_program_counter(self.ram_offset as u16 + register_value as u16);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_program_counter(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct CallSubroutine {
    ram_offset: u16,
    previous_value: u16
}

impl CallSubroutine {
    pub fn new(ram_offset: u16) -> CallSubroutine {
        CallSubroutine {
            ram_offset,
            previous_value: 0
        }
    }

}

impl Executable for CallSubroutine {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        cpu.push_to_stack(cpu.get_program_counter_value());
        cpu.set_program_counter(self.ram_offset - 2);
        // cpu.set_program_counter(self.ram_offset);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_program_counter(self.previous_value );
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct ReturnFromSubroutine {
    previous_value: u16
}

impl ReturnFromSubroutine {
    pub fn new() -> ReturnFromSubroutine {
        ReturnFromSubroutine {
            previous_value: 0
        }
    }
}

impl Executable for ReturnFromSubroutine {
    fn execute(&mut self, cpu: &mut CPU, _ram: &mut RAM, _vram: &mut VRAM) -> bool {
        cpu.resume_from_stack();
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let _popped_value = cpu.get_stack_head();
        cpu.set_program_counter(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct ExecuteInstructionFromRAM {
    offset: u16
}

impl ExecuteInstructionFromRAM {
    pub fn new(offset: u16) -> ExecuteInstructionFromRAM {
        ExecuteInstructionFromRAM {
            offset
        }
    }
}

impl Executable for ExecuteInstructionFromRAM {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, vram: &mut VRAM) -> bool {

        let opcode = ram.read_word(self.offset as usize);
        if opcode == 0x00 {
            panic!("Aborting on null command");
        }
        
        InstructionExecutor::execute(cpu, ram, opcode, vram);
        return true;
    }

    fn rollback(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {
        
    }

    fn record_value(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {
        
    }
}

pub struct SkipIfRegisterEqualsValue {
    source_register: CPURegister,
    value: u8,
    previous_value: u16,
    did_skip: bool
}

impl SkipIfRegisterEqualsValue {
    fn new(source_register: CPURegister, value: u8) -> SkipIfRegisterEqualsValue {
        SkipIfRegisterEqualsValue {
            source_register: source_register,
            value: value,
            previous_value: 0,
            did_skip: false
        }
    }
}

impl Executable for SkipIfRegisterEqualsValue {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        if cpu.get_register_value(&self.source_register).unwrap() == self.value {
            cpu.inc_program_counter();
            self.did_skip = true;
        }
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        if self.did_skip {
            cpu.set_program_counter(self.previous_value);
        }
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct SkipIfRegisterEqualsRegister {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u16,
    did_skip: bool
}

impl SkipIfRegisterEqualsRegister {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> SkipIfRegisterEqualsRegister {
        SkipIfRegisterEqualsRegister {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0,
            did_skip: false
        }
    }
}

impl Executable for SkipIfRegisterEqualsRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register_value = cpu.get_register_value(&self.destination_register).unwrap();

        if source_register_value == destination_register_value {
            cpu.inc_program_counter();
            self.did_skip = true;
        }
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        if self.did_skip {
            cpu.set_program_counter(self.previous_value);
        }
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct SkipIfRegisterNotEqualsValue {
    source_register: CPURegister,
    value: u8,
    previous_value: u16,
    did_skip: bool
}

impl SkipIfRegisterNotEqualsValue {
    fn new(source_register: CPURegister, value: u8) -> SkipIfRegisterNotEqualsValue {
        SkipIfRegisterNotEqualsValue {
            source_register: source_register,
            value: value,
            previous_value: 0,
            did_skip: false
        }
    }
}

impl Executable for SkipIfRegisterNotEqualsValue {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        println!("Register Value: {:?}", cpu.get_register_value(&self.source_register).unwrap());
        println!("Match Value: {:?}", self.value);
        if cpu.get_register_value(&self.source_register).unwrap() != self.value {
            cpu.inc_program_counter();
            self.did_skip = true;
        }
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        if self.did_skip {
            cpu.set_program_counter(self.previous_value);
        }
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct SkipIfRegisterNotEqualsRegister {
    source_register: CPURegister,
    destination_register: CPURegister,
    previous_value: u16,
    did_skip: bool
}

impl SkipIfRegisterNotEqualsRegister {
    fn new(source_register: CPURegister, destination_register: CPURegister) -> SkipIfRegisterNotEqualsRegister {
        SkipIfRegisterNotEqualsRegister {
            source_register: source_register,
            destination_register: destination_register,
            previous_value: 0,
            did_skip: false
        }
    }
}

impl Executable for SkipIfRegisterNotEqualsRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        let destination_register_value = cpu.get_register_value(&self.destination_register).unwrap();

        if source_register_value != destination_register_value {
            cpu.inc_program_counter();
            self.did_skip = true;
        }
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        if self.did_skip {
            cpu.set_program_counter(self.previous_value);
        }
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct SetDelayTimerToRegister {
    source_register: CPURegister,
    previous_value: u8
}

impl SetDelayTimerToRegister {
    fn new(source_register: CPURegister) -> SetDelayTimerToRegister {
        SetDelayTimerToRegister {
            source_register: source_register,
            previous_value: 0
        }
    }
}

impl Executable for SetDelayTimerToRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let value = cpu.get_register_value(&self.source_register).unwrap();
        cpu.set_delay_timer(value);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_delay_timer(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_delay_timer_value();
    }
}

pub struct SetRegisterToDelayTimer {
    source_register: CPURegister,
    previous_value: u8
}

impl SetRegisterToDelayTimer {
    fn new(source_register: CPURegister) -> SetRegisterToDelayTimer {
        SetRegisterToDelayTimer {
            source_register: source_register,
            previous_value: 0
        }
    }
}

impl Executable for SetRegisterToDelayTimer {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let delay_timer_value = cpu.get_delay_timer_value();
        let register = cpu.get_register_as_mut(&self.source_register).unwrap();
        register.set_value_as_u8(delay_timer_value);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register = cpu.get_register_as_mut(&self.source_register).unwrap();
        register.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_value(&self.source_register).unwrap();
    }
}

pub struct SetSoundTimerToRegister {
    source_register: CPURegister,
    previous_value: u8
}

impl SetSoundTimerToRegister {
    fn new(source_register: CPURegister) -> SetSoundTimerToRegister {
        SetSoundTimerToRegister {
            source_register: source_register,
            previous_value: 0
        }
    }
}

impl Executable for SetSoundTimerToRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let value = cpu.get_register_value(&self.source_register).unwrap();
        cpu.set_sound_timer(value);
        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_sound_timer(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_sound_timer_value();
    }
}

pub struct HaltForKeyPress {
    source_register: CPURegister,
    previous_value: u8
}

impl HaltForKeyPress {
    fn new(source_register: CPURegister) -> HaltForKeyPress {
        HaltForKeyPress {
            source_register,
            previous_value: 0
        }
    }
}

impl Executable for HaltForKeyPress {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        if cpu.any_key_is_pressed() {
            let first_key_pressed = cpu.first_pressed_key().unwrap();
            let register_handle = cpu.get_register_as_mut(&self.source_register).unwrap();

            register_handle.set_value_as_u8(first_key_pressed);
            return true;
        }

        let previous_program_counter = cpu.get_program_counter_value();
        cpu.set_program_counter(previous_program_counter - 2);

        false
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        let register_handle = cpu.get_register_as_mut(&self.source_register).unwrap();
        register_handle.set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_value(&self.source_register).unwrap();
    }
}

pub struct SkipIfKeyPressEqualsRegister {
    source_register: CPURegister,
    previous_value: u16
}

impl SkipIfKeyPressEqualsRegister {
    fn new(source_register: CPURegister) -> SkipIfKeyPressEqualsRegister {
        SkipIfKeyPressEqualsRegister {
            source_register: source_register,
            previous_value: 0
        }
    }
}

impl Executable for SkipIfKeyPressEqualsRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        if cpu.any_key_is_pressed() {
            let register_value = cpu.get_register_value(&self.source_register).unwrap();
            if cpu.key_is_pressed(register_value) {
                cpu.inc_program_counter();
            }
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_program_counter(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct SkipIfKeyPressNotEqualsRegister {
    source_register: CPURegister,
    previous_value: u16
}

impl SkipIfKeyPressNotEqualsRegister {
    fn new(source_register: CPURegister) -> SkipIfKeyPressNotEqualsRegister {
        SkipIfKeyPressNotEqualsRegister {
            source_register: source_register,
            previous_value: 0
        }
    }
}

impl Executable for SkipIfKeyPressNotEqualsRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        if cpu.any_key_is_pressed() {
            let register_value = cpu.get_register_value(&self.source_register).unwrap();
            if cpu.key_is_pressed(register_value) {
                cpu.inc_program_counter();
            }
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_program_counter(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_program_counter_value();
    }
}

pub struct StoreInIRegister {
    value: u16,
    previous_value: u16
}

impl StoreInIRegister {
    pub fn new(value: u16) -> StoreInIRegister {
        StoreInIRegister {
            value,
            previous_value: 0
        }
    }
}

impl Executable for StoreInIRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        cpu.set_i_register_value(self.value);
        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_i_register_value(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_i_register_value_as_u16();
    }
}

pub struct AddToIRegister {
    register: CPURegister,
    previous_value: u16
}

impl AddToIRegister {
    pub fn new(register: CPURegister) -> AddToIRegister {
        AddToIRegister {
            register,
            previous_value: 0
        }
    }
}

impl Executable for AddToIRegister {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        cpu.add_to_i_register_value((cpu.get_register_value(&self.register).unwrap()) as u16);
        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_i_register_value(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_i_register_value_as_u16();
    }
}

pub struct DrawSpriteData {
    x_offset_register: CPURegister,
    y_offset_register: CPURegister,
    bytes_of_data: u8,
    previous_value: u8
}

impl DrawSpriteData {
    pub fn new(x_offset_register: CPURegister, y_offset_register: CPURegister, bytes_of_data: u8) -> DrawSpriteData {
        DrawSpriteData {
            x_offset_register,
            y_offset_register,
            bytes_of_data,
            previous_value: 0
        }
    }
}

impl Executable for DrawSpriteData {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let start_x_offset = cpu.get_register_value(&self.x_offset_register).unwrap() % 64;
        let start_y_offset = cpu.get_register_value(&self.y_offset_register).unwrap() % 32;
        cpu.get_register_as_mut(&CPURegister::from_u8(15))
            .unwrap()
            .set_value_as_u8(0);
        
        for y_offset in 0..self.bytes_of_data {

            let ram_address = (cpu.get_i_register_value_as_u16() as usize) + (y_offset as usize);
            let ram_value = ram.read_byte(ram_address);
            
            if start_y_offset + y_offset <= VRAM::get_window_height() {
                let set_flags_register = vram.print_sprite_row(start_x_offset, start_y_offset + y_offset, ram_value);
                if set_flags_register {
                    cpu.get_register_as_mut(&CPURegister::from_u8(15))
                        .unwrap()
                        .set_value_as_u8(1);
                }
            }
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.get_register_as_mut(&CPURegister::from_u8(0xF))
            .unwrap()
            .set_value_as_u8(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_register_value(&CPURegister::from_u8(0xF)).unwrap();
    }
}

pub struct ClearScreen {

}

impl ClearScreen {
    pub fn new() -> ClearScreen {
        ClearScreen { }
    }
}

impl Executable for ClearScreen {
    fn execute(&mut self, _cpu: &mut CPU, _ram: &mut RAM, vram: &mut VRAM) -> bool {
        vram.clear_screen();
        true
    }

    fn rollback(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {
        // no way to roll back
    }

    fn record_value(&mut self, _cpu: &mut CPU, _ram: &mut RAM) {
        // nothing to record
    }
}

pub struct SetIRegisterToSpriteAddress {
    source_register: CPURegister,
    previous_value: u16
}

impl SetIRegisterToSpriteAddress {
    pub fn new(register: CPURegister) -> SetIRegisterToSpriteAddress {
        SetIRegisterToSpriteAddress {
            source_register: register,
            previous_value: 0
        }
    }
}

impl Executable for SetIRegisterToSpriteAddress {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let source_register_value = cpu.get_register_value(&self.source_register).unwrap();
        
        cpu.set_i_register_value(RAM::get_location_for_sprite(source_register_value) as u16);

        return true;
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_i_register_value(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_i_register_value_as_u16();
    }
}

pub struct DecimalEncode {
    source_register: CPURegister,
    previous_value: [u8; 3]
}

impl DecimalEncode {
    pub fn new(source_register: CPURegister) -> DecimalEncode {
        DecimalEncode {
            source_register,
            previous_value: [0, 0, 0]
        }
    }
}

impl Executable for DecimalEncode {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);
        let ram_offset = cpu.get_i_register_value_as_u16();
        let register_value = cpu.get_register_value(&self.source_register).unwrap();

        let hundreds = (register_value - (register_value % 100)) / 100;
        let units = register_value % 10;
        let tens = ((register_value - (hundreds * 100)) - (units)) / 10;

        if let Err(_) = ram.save_byte(hundreds, ram_offset as usize) {
            return false;
        }
        if let Err(_) = ram.save_byte(tens, (ram_offset + 1) as usize) {
            return false;
        }
        
        if let Err(_) = ram.save_byte(units, (ram_offset + 2) as usize) {
            return false;
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, ram: &mut RAM) {
        let address = cpu.get_i_register_value_as_u16();
        if let Err(_) = ram.save_byte(self.previous_value[0], address as usize) {
            panic!("Cannot set memory address!");
        }

        if let Err(_) = ram.save_byte(self.previous_value[1], address as usize) {
            panic!("Cannot set memory address!");
        }
        
        if let Err(_) = ram.save_byte(self.previous_value[2], address as usize) {
            panic!("Cannot set memory address!");
        }
    }

    fn record_value(&mut self, cpu: &mut CPU, ram: &mut RAM) {
        let address = cpu.get_register_value(&self.source_register).unwrap();
        self.previous_value = [ram.read_byte(address as usize), ram.read_byte((address + 1) as usize), ram.read_byte((address + 2) as usize)];
    }
}

pub struct StoreRegisterValuesInRAM {
    source_register: CPURegister,
    previous_value: u16
}

impl StoreRegisterValuesInRAM {
    pub fn new(source_register: CPURegister) -> StoreRegisterValuesInRAM {
        StoreRegisterValuesInRAM {
            source_register,
            previous_value: 0
        }
    }
}

impl Executable for StoreRegisterValuesInRAM {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let registers = CPURegister::registers_below(&self.source_register);
        let address = cpu.get_i_register_value_as_u16();

        for (index, register) in registers.iter().enumerate() {
            let save_result = ram.save_byte(cpu.get_register_value(&register).unwrap(), (address + (index as u16)) as usize);
            if let Err(text) = save_result {
                panic!("{}", text);
            }
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_i_register_value(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_i_register_value_as_u16();
    }
}

pub struct LoadRegistersFromRAM {
    source_register: CPURegister,
    previous_value: u16
}

impl LoadRegistersFromRAM {
    pub fn new(source_register: CPURegister) -> LoadRegistersFromRAM {
        LoadRegistersFromRAM {
            source_register,
            previous_value: 0
        }
    }
}

impl Executable for LoadRegistersFromRAM {
    fn execute(&mut self, cpu: &mut CPU, ram: &mut RAM, _vram: &mut VRAM) -> bool {
        self.record_value(cpu, ram);

        let registers = CPURegister::registers_below(&self.source_register);
        let address = cpu.get_i_register_value_as_u16();

        for (index, register) in registers.iter().enumerate() {
            cpu.get_register_as_mut(&register)
                .unwrap()
                .set_value_as_u8(ram.read_byte((address + (index as u16)) as usize));
        }

        true
    }

    fn rollback(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        cpu.set_i_register_value(self.previous_value);
    }

    fn record_value(&mut self, cpu: &mut CPU, _ram: &mut RAM) {
        self.previous_value = cpu.get_i_register_value_as_u16();
    }
}