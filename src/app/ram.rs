use crate::app::font::Font;

const RAM_FONT_LOCATION: u16 = 0x50;

pub struct RAM {
    memory: [u8; 0x0FFF]
}

impl RAM {
    pub fn new() -> RAM {
        RAM {
            memory: [0; 0x0FFF]
        }
    }

    pub fn save_byte(&mut self, byte: u8, offset: usize) -> Result<(), &str> {
        if offset > 0x0FFE {
            return Err("Invalid RAM Offset");
        }
        self.memory[offset] = byte;
        Ok(())
    }

    pub fn read_byte(&self, byte: usize) -> u8 {
        if byte > 0x0FFF {
            return 0;
        }

        self.memory[byte]
    }

    pub fn read_word(&self, word: usize) -> u16 {
        let upper: u16 = self.read_byte(word) as u16;
        let lower: u16  = self.read_byte(word + 1) as u16;
        
        (upper << 8) + lower
    }

    pub fn get_font_ram_location() -> u16 {
        return RAM_FONT_LOCATION;
    }

    pub fn get_location_for_sprite(register_value: u8) -> usize {
        let font_ram_offset: usize;
        match register_value {
            0x0     => { font_ram_offset = Font::get_offset_for_0() },
            0x1     => { font_ram_offset = Font::get_offset_for_1() },
            0x2     => { font_ram_offset = Font::get_offset_for_2() },
            0x3     => { font_ram_offset = Font::get_offset_for_3() },
            0x4     => { font_ram_offset = Font::get_offset_for_4() },
            0x5     => { font_ram_offset = Font::get_offset_for_5() },
            0x6     => { font_ram_offset = Font::get_offset_for_6() },
            0x7     => { font_ram_offset = Font::get_offset_for_7() },
            0x8     => { font_ram_offset = Font::get_offset_for_8() },
            0x9     => { font_ram_offset = Font::get_offset_for_9() },
            0xa     => { font_ram_offset = Font::get_offset_for_a() },
            0xb     => { font_ram_offset = Font::get_offset_for_b() },
            0xc     => { font_ram_offset = Font::get_offset_for_c() },
            0xd     => { font_ram_offset = Font::get_offset_for_d() },
            0xe     => { font_ram_offset = Font::get_offset_for_e() },
            0xf     => { font_ram_offset = Font::get_offset_for_f() },
            _       => { font_ram_offset = 0 }
        };

        return RAM_FONT_LOCATION as usize + font_ram_offset;
    }

    pub fn print_offset(&self, offset: usize)
    {
        
    }
}