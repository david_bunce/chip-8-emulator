use std::fmt::Debug;
use std::collections::HashMap;
use std::collections::hash_map::Iter;
use crate::app::stack::Stack;
use crate::app::instruction::Executable;

#[derive(Debug)]
pub struct ByteRegister {
    value: u8
}

impl ByteRegister {
    pub fn new() -> ByteRegister {
        ByteRegister {
            value: 0
        }
    }

    pub fn set_value_as_u8(&mut self, value: u8) {
        self.value = value;
    }

    pub fn add(&mut self, adduend: u8) -> (u8, bool) {
        let sum = self.value.overflowing_add(adduend);
        self.set_value_as_u8(sum.0);
        sum
    }

    pub fn subtract(&mut self, subtrahend: u8) -> (u8, bool) {
        let underflow = subtrahend > self.value;
        self.set_value_as_u8(self.value.wrapping_sub(subtrahend));

        (self.value, underflow)
    }

    pub fn as_u8(&self) -> u8 {
        self.value
    }

    pub fn as_i8(&self) -> i8 {
        self.value as i8
    }
}

pub struct WordRegister {
    value: u16
}

impl WordRegister {
    pub fn new() -> WordRegister {
        WordRegister {
            value: 0 as u16
        }
    }

    pub fn set_value_as_u16(&mut self, value: u16) {
        self.value = value;
    }

    pub fn add(&mut self, adduend: u16) -> (u16, bool) {
        self.value.overflowing_add(adduend)
    }

    pub fn subtract(&mut self, minuend: u16) -> (u16, bool) {
        self.value.overflowing_sub(minuend)
    }

    pub fn as_u16(&self) -> u16 {
        self.value
    }
}

struct CPUHistory {
    history: Vec<Box<Executable>>
}

impl CPUHistory {
    pub fn add(&mut self, instruction: Box<Executable>) {
        self.history.push(instruction);
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum CPURegister {
    V0,
    V1,
    V2,
    V3,
    V4,
    V5,
    V6,
    V7,
    V8,
    V9,
    VA,
    VB,
    VC,
    VD,
    VE,
    VF,
    VI,
    VX,
    None
}

impl CPURegister {
    pub fn from_string_letter(register: &str) -> CPURegister {
        match register {
            "0" => { CPURegister::V0 },
            "1" => { CPURegister::V1 },
            "2" => { CPURegister::V2 },
            "3" => { CPURegister::V3 },
            "4" => { CPURegister::V4 },
            "5" => { CPURegister::V5 },
            "6" => { CPURegister::V6 },
            "7" => { CPURegister::V7 },
            "8" => { CPURegister::V8 },
            "9" => { CPURegister::V9 },
            "A" => { CPURegister::VA },
            "B" => { CPURegister::VB },
            "C" => { CPURegister::VC },
            "D" => { CPURegister::VD },
            "E" => { CPURegister::VE },
            "F" => { CPURegister::VF },
            "I" => { CPURegister::VI },
            "X" => { CPURegister::VX },
            _ => { CPURegister::None }
        }
    }

    pub fn from_u8(register: u8) -> CPURegister {
        match register {
            0 => { CPURegister::V0 },
            1 => { CPURegister::V1 },
            2 => { CPURegister::V2 },
            3 => { CPURegister::V3 },
            4 => { CPURegister::V4 },
            5 => { CPURegister::V5 },
            6 => { CPURegister::V6 },
            7 => { CPURegister::V7 },
            8 => { CPURegister::V8 },
            9 => { CPURegister::V9 },
            10 => { CPURegister::VA },
            11 => { CPURegister::VB },
            12 => { CPURegister::VC },
            13 => { CPURegister::VD },
            14 => { CPURegister::VE },
            15 => { CPURegister::VF },
            16 => { CPURegister::VI },
            17 => { CPURegister::VX },
            _ => { CPURegister::None }
        }
    }

    fn u8_from(register: &CPURegister) -> u8 {
        match *register {
            CPURegister::V0 => { 0x0 },
            CPURegister::V1 => { 0x1 },
            CPURegister::V2 => { 0x2 },
            CPURegister::V3 => { 0x3 },
            CPURegister::V4 => { 0x4 },
            CPURegister::V5 => { 0x5 },
            CPURegister::V6 => { 0x6 },
            CPURegister::V7 => { 0x7 },
            CPURegister::V8 => { 0x8 },
            CPURegister::V9 => { 0x9 },
            CPURegister::VA => { 0xA },
            CPURegister::VB => { 0xB },
            CPURegister::VC => { 0xC },
            CPURegister::VD => { 0xD },
            CPURegister::VE => { 0xE },
            CPURegister::VF => { 0xF },
            _               => { 0x0 }
        }
    }

    pub fn registers_below(register: &CPURegister) -> Vec<CPURegister> {
        
        let vec_of_register_names : Vec<CPURegister> = CPURegister::registers_as_vector();
        let mut registers = vec![];

        for register_name in vec_of_register_names {
            let is_last_register = register_name == *register;
            registers.push(register_name);
            if is_last_register {
                break;
            }
        }

        registers
    }

    fn registers_as_vector() -> Vec<CPURegister> {
        vec![
            CPURegister::V0,
            CPURegister::V1,
            CPURegister::V2,
            CPURegister::V3,
            CPURegister::V4,
            CPURegister::V5,
            CPURegister::V6,
            CPURegister::V7,
            CPURegister::V8,
            CPURegister::V9,
            CPURegister::VA,
            CPURegister::VB,
            CPURegister::VC,
            CPURegister::VD,
            CPURegister::VE,
            CPURegister::VF,
        ]
    }
}




