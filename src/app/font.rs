pub struct Font {}

impl Font {
    pub fn get_font_data() -> Vec<u8> {
        vec![
            0xF0, 0x90, 0x90, 0x90, 0xF0,
            0x20, 0x60, 0x20, 0x20, 0x70,
            0xF0, 0x10, 0xF0, 0x80, 0xF0,
            0xF0, 0x10, 0xF0, 0x10, 0xF0,
            0x90, 0x90, 0xF0, 0x10, 0x10,
            0xF0, 0x80, 0xF0, 0x10, 0xF0,
            0xF0, 0x80, 0xF0, 0x90, 0xF0,
            0xF0, 0x10, 0x20, 0x40, 0x40,
            0xF0, 0x90, 0xF0, 0x90, 0xF0,
            0xF0, 0x90, 0xF0, 0x10, 0xF0,
            0xF0, 0x90, 0xF0, 0x90, 0x90,
            0xE0, 0x90, 0xE0, 0x90, 0xE0,
            0xF0, 0x80, 0x80, 0x80, 0xF0,
            0xE0, 0x90, 0x90, 0x90, 0xE0,
            0xF0, 0x80, 0xF0, 0x80, 0xF0,
            0xF0, 0x80, 0xF0, 0x80, 0x80 
        ]
    }

    pub fn get_offset_for_0() -> usize {
        return 0;
    }

    pub fn get_offset_for_1() -> usize {
        return 5;
    }

    pub fn get_offset_for_2() -> usize {
        return 10;
    }

    pub fn get_offset_for_3() -> usize {
        return 15;
    }

    pub fn get_offset_for_4() -> usize {
        return 20;
    }

    pub fn get_offset_for_5() -> usize {
        return 25;
    }

    pub fn get_offset_for_6() -> usize {
        return 30;
    }

    pub fn get_offset_for_7() -> usize {
        return 35;
    }

    pub fn get_offset_for_8() -> usize {
        return 40;
    }

    pub fn get_offset_for_9() -> usize {
        return 45;
    }

    pub fn get_offset_for_a() -> usize {
        return 50;
    }

    pub fn get_offset_for_b() -> usize {
        return 55;
    }

    pub fn get_offset_for_c() -> usize {
        return 60;
    }

    pub fn get_offset_for_d() -> usize {
        return 65;
    }

    pub fn get_offset_for_e() -> usize {
        return 70;
    }

    pub fn get_offset_for_f() -> usize {
        return 75;
    }
}